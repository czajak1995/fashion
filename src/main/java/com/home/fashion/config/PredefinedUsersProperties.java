package com.home.fashion.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Data
@Component
@ConfigurationProperties(prefix = "app")
public class PredefinedUsersProperties {
    private List<UserProperties> predefinedUsers = new ArrayList<>();
}
