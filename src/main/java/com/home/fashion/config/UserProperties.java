package com.home.fashion.config;

import lombok.Data;

@Data
public class UserProperties {
    private String name;
    private String avatar;
}
