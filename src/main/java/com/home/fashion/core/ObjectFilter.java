package com.home.fashion.core;

import com.home.fashion.core.annotations.IgnoreFilter;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.reflect.FieldUtils;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

@Data
@Slf4j
@AllArgsConstructor
@NoArgsConstructor
public class ObjectFilter implements Serializable {

    private Map<String, Object> filters = new HashMap<>();

    public Map<String, ObjectProperties> getFilters(Class<?> objectType) {
        return Arrays.stream(FieldUtils.getAllFields(objectType))
                .filter(field -> !Modifier.isStatic(field.getModifiers()))
                .filter(field -> filters.containsKey(field.getName()))
                .filter(field -> !isIgnored(field))
                .collect(Collectors.toMap(Field::getName, field -> new ObjectProperties(filters.get(field.getName()))));
    }

    private static boolean isIgnored(Field field) {
        return field.isAnnotationPresent(IgnoreFilter.class) && field.getAnnotation(IgnoreFilter.class).value();
    }

}
