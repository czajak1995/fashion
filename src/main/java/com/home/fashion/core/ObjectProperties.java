package com.home.fashion.core;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ObjectProperties {
    private Object value;
}
