package com.home.fashion.core.annotations;

public @interface IgnoreFilter {
    boolean value() default true;
}
