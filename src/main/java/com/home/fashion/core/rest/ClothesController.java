package com.home.fashion.core.rest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.home.fashion.core.ObjectFilter;
import com.home.fashion.model.Clothes;
import com.home.fashion.model.ClothesDetails;
import com.home.fashion.model.GalleryItem;
import com.home.fashion.service.ClothesService;
import com.home.fashion.service.UploadService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@AllArgsConstructor
@RestController
@RequestMapping("/api/clothes")
public class ClothesController {

    private final ObjectMapper objectMapper;
    private final ClothesService clothesService;
    private final UploadService uploadService;

    @PostMapping("/filtered")
    public Iterable<Clothes> getClothes(@RequestBody ObjectFilter filter) {
        return clothesService.getFiltered(filter);
    }

    @GetMapping("/{id}")
    public Clothes getClothes(@PathVariable int id) {
        return clothesService.getClothes(id);
    }

    @PostMapping
    public Clothes insertClothes(@RequestBody ClothesDetails clothesDetails) {
        return clothesService.insertClothes(clothesDetails);
    }

    @GetMapping("/gallery/{id}/data.json")
    public Iterable<GalleryItem> getGalleryItems(HttpServletRequest request, @PathVariable int id) {
        return clothesService.getGalleryItems(id, getImageUrl(request));
    }

    @GetMapping("/images/{name}")
    public byte[] getImage(@PathVariable String name) {
        return clothesService.getImage(name);
    }

    private static String getImageUrl(HttpServletRequest request) {
        return request.getScheme() + "://" + request.getServerName() + ":" + request.getLocalPort() + "/api/clothes/images/%s";
    }
}
