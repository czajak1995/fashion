package com.home.fashion.core.rest;

import com.fasterxml.jackson.databind.JsonNode;
import com.home.fashion.service.ModelInfoProvider;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/ui")
@RequiredArgsConstructor
public class UiController {

    private final ModelInfoProvider modelInfoProvider;

    @GetMapping("/enums")
    public JsonNode getEnums() {
        return modelInfoProvider.getEnums();
    }
}
