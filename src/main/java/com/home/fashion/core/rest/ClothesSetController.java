package com.home.fashion.core.rest;

import com.home.fashion.core.ObjectFilter;
import com.home.fashion.model.ClothesSet;
import com.home.fashion.service.ClothesSetService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
@RequestMapping("/api/clothes-set")
public class ClothesSetController {

    private final ClothesSetService clothesSetService;

    @PostMapping
    public Iterable<ClothesSet> getClothesSet(@RequestBody ObjectFilter filter) {
        return clothesSetService.getFiltered(filter);
    }

    @PostMapping("/new")
    public ClothesSet insertClothes(@RequestBody ClothesSet clothes) {
        return clothesSetService.insertClothesSet(clothes);
    }

    @GetMapping
    public ClothesSet getClothes(@RequestParam int id) {
        return clothesSetService.getClothesSet(id);
    }
}
