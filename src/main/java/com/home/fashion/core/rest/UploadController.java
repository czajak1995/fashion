package com.home.fashion.core.rest;

import com.home.fashion.model.ClothesResource;
import com.home.fashion.model.ClothesSetResource;
import com.home.fashion.service.UploadService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import java.util.Iterator;
import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/upload")
public class UploadController {

    private final UploadService uploadService;

    @PostMapping("/clothes")
    public ClothesResource uploadClothes(@RequestParam("file_data") MultipartFile resource) {
        return uploadService.saveClothes(resource);
    }

    @PostMapping("/clothes-set")
    public String uploadClothesSet(@RequestBody MultipartFile resource) {
        return uploadService.saveClothesSet(resource);
    }
}
