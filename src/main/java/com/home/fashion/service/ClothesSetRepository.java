package com.home.fashion.service;

import com.home.fashion.model.ClothesSet;
import org.springframework.data.repository.CrudRepository;

public interface ClothesSetRepository extends CrudRepository<ClothesSet, Integer> {
}
