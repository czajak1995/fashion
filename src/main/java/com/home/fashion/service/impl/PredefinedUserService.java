package com.home.fashion.service.impl;

import com.home.fashion.config.PredefinedUsersProperties;
import com.home.fashion.config.UserProperties;
import com.home.fashion.model.User;
import com.home.fashion.service.UserRepository;
import com.home.fashion.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
@AllArgsConstructor
public class PredefinedUserService implements UserService {

    private final PredefinedUsersProperties usersProperties;
    private final UserRepository userRepository;

    @PostConstruct
    public void createPredefinedUsers() {
        usersProperties.getPredefinedUsers().forEach(this::createPredefinedUser);
    }

    public void createPredefinedUser(UserProperties userProperties) {
        if (!userRepository.existsById(userProperties.getName())) {
            User user = new User();
            user.setName(userProperties.getName());
            user.setAvatar(userProperties.getAvatar().getBytes());
            userRepository.save(user);
        }
    }
}
