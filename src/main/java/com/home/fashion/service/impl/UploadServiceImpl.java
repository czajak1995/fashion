package com.home.fashion.service.impl;

import com.home.fashion.model.ClothesResource;
import com.home.fashion.model.ClothesSetResource;
import com.home.fashion.service.ClothesResourceRepository;
import com.home.fashion.service.ClothesSetResourceRepository;
import com.home.fashion.service.UploadService;
import com.home.fashion.service.utils.ImageDominantColor;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Objects;

@Service
@AllArgsConstructor
public class UploadServiceImpl implements UploadService {

    private final ClothesResourceRepository clothesResourceRepository;
    private final ClothesSetResourceRepository clothesSetResourceRepository;

    @Override
    public ClothesResource saveClothes(MultipartFile resource) {
        String resourceName = Objects.requireNonNull(resource.getOriginalFilename());

        ClothesResource clothesResource = clothesResourceRepository.findById(resourceName)
                .orElse(ClothesResource.emptyResource(resourceName));
        clothesResource.setResource(getBytes(resource));
        BufferedImage image = readImage(resource);
        clothesResource.setWidth(image.getWidth());
        clothesResource.setHeight(image.getHeight());
        clothesResource.setDominantColor(ImageDominantColor.getDominantColor(image));
        return clothesResourceRepository.save(clothesResource);
    }

    @Override
    public String saveClothesSet(MultipartFile image) {
        String resourceName = Objects.requireNonNull(image.getOriginalFilename());

        ClothesSetResource resource = clothesSetResourceRepository.findById(resourceName)
                .orElse(ClothesSetResource.emptyResource(resourceName));
        resource.setResource(getBytes(image));
        return clothesSetResourceRepository.save(resource).getName();
    }

    private byte[] getBytes(MultipartFile resource) {
        String resourceName = StringUtils.cleanPath(Objects.requireNonNull(resource.getOriginalFilename()));
        if (isInvalid(resourceName)) {
            throw new IllegalArgumentException("Resource contains invalid path sequence " + resourceName);
        }

        try {
            return resource.getBytes();
        } catch (IOException e) {
            throw new IllegalArgumentException(String.format("Could not store resource %s. Please try again!", resourceName), e);
        }
    }

    private boolean isInvalid(String resourceName) {
        return resourceName.contains("..");
    }

    private static BufferedImage readImage(MultipartFile resource) {
        try {
            return ImageIO.read(resource.getInputStream());
        } catch (IOException e) {
            throw new IllegalArgumentException("Cannot read image with name " + resource.getName());
        }
    }
}
