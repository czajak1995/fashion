package com.home.fashion.service.impl;

import com.google.common.collect.Sets;
import com.home.fashion.core.ObjectFilter;
import com.home.fashion.core.ObjectProperties;
import com.home.fashion.model.Clothes;
import com.home.fashion.model.ClothesDetails;
import com.home.fashion.model.ClothesResource;
import com.home.fashion.model.GalleryItem;
import com.home.fashion.model.User;
import com.home.fashion.service.ClothesRepository;
import com.home.fashion.service.ClothesResourceRepository;
import com.home.fashion.service.ClothesService;
import com.home.fashion.service.UserRepository;
import com.home.fashion.service.utils.QueryExecutor;
import com.home.fashion.service.utils.QueryExecutorImpl;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

import static com.home.fashion.service.utils.CustomCollectors.intersecting;

@Service
public class ClothesServiceImpl implements ClothesService {

    private final ClothesRepository clothesRepository;
    private final ClothesResourceRepository clothesResourceRepository;
    private final UserRepository userRepository;
    private final QueryExecutor queryExecutor;

    public ClothesServiceImpl(ClothesRepository clothesRepository,
                              ClothesResourceRepository clothesResourceRepository,
                              UserRepository userRepository,
                              EntityManager entityManager) {
        this.clothesRepository = clothesRepository;
        this.clothesResourceRepository = clothesResourceRepository;
        this.userRepository = userRepository;
        this.queryExecutor = new QueryExecutorImpl<>(Clothes.class, entityManager);
    }

    @Transactional
    public Iterable<Clothes> getFiltered(ObjectFilter filter) {
        var filters = filter.getFilters(Clothes.class);

        if (filters.isEmpty()) {
            return clothesRepository.findAll();
        }
        return filters.entrySet().stream()
                .map(entry -> Sets.newHashSet(findClothes(entry.getKey(), entry.getValue())))
                .collect(intersecting());
    }

    @Override
    @Transactional
    public Iterable<GalleryItem> getGalleryItems(int id, String url) {
        return clothesRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Cannot find clothes with id " + id))
                .getResources().stream()
                .map(resource -> new GalleryItem(resource.getName(), resource.getWidth(), resource.getHeight(), resource.getDominantColor(), url))
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public Clothes insertClothes(ClothesDetails clothesDetails) {
        return clothesRepository.save(toClothes(clothesDetails));
    }

    @Override
    @Transactional
    public Clothes getClothes(int id) {
        return clothesRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Cannot find clothes with id " + id));
    }

    @Override
    @Transactional
    public byte[] getImage(String name) {
        return clothesResourceRepository.findById(name)
                .orElseThrow(() -> new IllegalArgumentException("Cannot find image with name " + name))
                .getResource();
    }

    private Iterable<Clothes> findClothes(String field, ObjectProperties properties) {
        return queryExecutor.executeFindQuery(field, properties);
    }

    private Clothes toClothes(ClothesDetails clothesDetails) {
        Clothes clothes = new Clothes();
        clothes.setColors(clothesDetails.getColors());
        clothes.setType(clothesDetails.getType());
        clothes.setOccasions(clothesDetails.getOccasions());
        clothes.setSeasons(clothesDetails.getSeasons());
        clothes.setTags(clothesDetails.getTags());
        clothes.setOwnRank(clothesDetails.getOwnRank());
        clothes.setDisabled(clothesDetails.isDisabled());
//        clothes.setPlace(clothesDetails.getPlace());
        clothes.setUser(getUser(clothesDetails.getUser()));
        clothes.setResources(getResources(clothesDetails.getResources()));
        return clothes;
    }

    private User getUser(String user) {
        return userRepository.findById(user).orElseThrow(() -> new IllegalArgumentException("Cannot find user with name " + user));
    }

    private List<ClothesResource> getResources(List<String> names) {
        return names.stream()
                .map(name -> clothesResourceRepository.findById(name).orElseThrow(() -> new IllegalArgumentException("Cannot find resource with id " + name)))
                .collect(Collectors.toList());
    }

}
