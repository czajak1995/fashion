package com.home.fashion.service.impl;

import com.google.common.collect.Sets;
import com.home.fashion.core.ObjectFilter;
import com.home.fashion.core.ObjectProperties;
import com.home.fashion.model.ClothesSet;
import com.home.fashion.service.ClothesSetRepository;
import com.home.fashion.service.ClothesSetService;
import com.home.fashion.service.utils.QueryExecutor;
import com.home.fashion.service.utils.QueryExecutorImpl;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import static com.home.fashion.service.utils.CustomCollectors.intersecting;

@Service
public class ClothesSetServiceImpl implements ClothesSetService {

    private final ClothesSetRepository clothesSetRepository;
    private final QueryExecutor<ClothesSet> queryExecutor;

    public ClothesSetServiceImpl(ClothesSetRepository clothesSetRepository, EntityManager entityManager) {
        this.clothesSetRepository = clothesSetRepository;
        this.queryExecutor = new QueryExecutorImpl<>(ClothesSet.class, entityManager);
    }

    @Transactional
    public Iterable<ClothesSet> getFiltered(ObjectFilter filter) {
        var filters = filter.getFilters(ClothesSet.class);
        if (filters.isEmpty()) {
            return clothesSetRepository.findAll();
        }

        return filters.entrySet().stream()
                .map(entry -> Sets.newHashSet(findClothesSet(entry.getKey(), entry.getValue())))
                .collect(intersecting());
    }

    @Override
    public ClothesSet insertClothesSet(ClothesSet clothes) {
        return clothesSetRepository.save(clothes);
    }

    @Override
    public ClothesSet getClothesSet(int id) {
        return clothesSetRepository.findById(id).orElseThrow();
    }

    private Iterable<ClothesSet> findClothesSet(String field, ObjectProperties properties) {
        return queryExecutor.executeFindQuery(field, properties);
    }
}
