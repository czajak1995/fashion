package com.home.fashion.service.impl;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.home.fashion.model.ClothesType;
import com.home.fashion.model.Color;
import com.home.fashion.model.Occasion;
import com.home.fashion.model.Season;
import com.home.fashion.service.ModelInfoProvider;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ModelInfoProviderImpl implements ModelInfoProvider {

    private final ObjectMapper objectMapper;
    private final List<Class<?>> enumClasses = List.of(
            ClothesType.class,
            Color.class,
            Occasion.class,
            Season.class
    );

    @Override
    public JsonNode getEnums() {
        Map<String, List<JsonNode>> enumsMap = enumClasses.stream()
                .collect(Collectors.toMap(Class::getSimpleName, this::getEnums));
        return objectMapper.valueToTree(enumsMap);
    }

    private List<JsonNode> getEnums(Class<?> clazz) {
        return Arrays.stream(clazz.getEnumConstants()).map(e -> (JsonNode) objectMapper.valueToTree(e)).collect(Collectors.toList());
    }


}
