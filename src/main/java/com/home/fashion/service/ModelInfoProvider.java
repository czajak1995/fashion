package com.home.fashion.service;

import com.fasterxml.jackson.databind.JsonNode;

public interface ModelInfoProvider {
    JsonNode getEnums();
}
