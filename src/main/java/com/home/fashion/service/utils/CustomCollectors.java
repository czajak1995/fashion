package com.home.fashion.service.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collector;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class CustomCollectors {
    public static <T, S extends Collection<T>> Collector<S, ?, Set<T>> intersecting() {
        class Acc {
            private Set<T> result;

            private void accept(S s) {
                if (result == null) result = new HashSet<>(s);
                else result.retainAll(s);
            }

            private Acc combine(Acc other) {
                if (result == null) return other;
                if (other.result != null) result.retainAll(other.result);
                return this;
            }
        }
        return Collector.of(Acc::new, Acc::accept, Acc::combine,
                acc -> acc.result == null ? Collections.emptySet() : acc.result,
                Collector.Characteristics.UNORDERED);
    }
}
