package com.home.fashion.service.utils;

class EnumConverter<T extends Enum<T>> {
    private Class<T> type;

    EnumConverter(Class<T> type) {
        this.type = type;
    }

    Enum<T> convert(String text) {
        for (Enum<T> candidate : type.getEnumConstants()) {
            if (candidate.name().equalsIgnoreCase(text)) {
                return candidate;
            }
        }
        return null;
    }
}