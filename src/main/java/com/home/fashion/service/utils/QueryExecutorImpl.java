package com.home.fashion.service.utils;

import com.home.fashion.core.ObjectProperties;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.reflect.FieldUtils;

import javax.persistence.EntityManager;
import javax.persistence.JoinTable;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static com.home.fashion.model.utils.EntityUtils.getTableName;

@RequiredArgsConstructor
public class QueryExecutorImpl<T> implements QueryExecutor<T> {

    private final Class<T> clazz;
    private final EntityManager entityManager;

    public List<T> executeFindQuery(String field, ObjectProperties properties) {
        if (isManyToManyRelation(field, clazz)) {
            return selectWhereManyToMany(getTableName(clazz), field, properties.getValue());
        }
        return selectWhereValue(clazz.getName(), field, properties.getValue());
    }

    private List<T> selectWhereValue(String table, String field, Object value) {
        String literalValue = toObjectLiteral(value);
        if (literalValue.isEmpty()) {
            return Collections.emptyList();
        }
        var sql = String.format("SELECT e FROM %s e WHERE %s in (%s)", table, field, literalValue);
        System.out.println(sql);
        return entityManager.createQuery(sql).getResultList();
    }

    private List<T> selectWhereManyToMany(String tableName, String field, Object value) {
        String literalValue = toObjectLiteral(value);
        if (literalValue.isEmpty()) {
            return Collections.emptyList();
        }
        var sql = String.format("SELECT m FROM %s m INNER JOIN m.%s n WHERE n in (%s)",
                tableName, field, literalValue);
        System.out.println(sql);
        return entityManager.createQuery(sql).getResultList();
    }

    private static <T> boolean isManyToManyRelation(String fieldName, Class<T> clazz) {
        return Arrays.stream(FieldUtils.getAllFields(clazz)).filter(field -> field.getName().equals(fieldName))
                .findFirst().map(field -> field.isAnnotationPresent(JoinTable.class)).orElse(false);
    }

    private static String toObjectLiteral(Object value) {
        if (value instanceof Collection) {
            return (String) ((Collection) value).stream()
                    .map(v -> toObjectLiteral(v))
                    .collect(Collectors.joining(","));
        } else if (value instanceof String) {
            return String.format("\'%s\'", value);
        }
        return value.toString();
    }
}
