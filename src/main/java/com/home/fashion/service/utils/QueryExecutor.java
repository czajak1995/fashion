package com.home.fashion.service.utils;

import com.home.fashion.core.ObjectProperties;

import java.util.List;

public interface QueryExecutor<T> {
    List<T> executeFindQuery(String field, ObjectProperties properties);
}
