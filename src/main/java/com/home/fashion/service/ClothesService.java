package com.home.fashion.service;

import com.home.fashion.core.ObjectFilter;
import com.home.fashion.model.Clothes;
import com.home.fashion.model.ClothesDetails;
import com.home.fashion.model.GalleryItem;

import java.util.List;

public interface ClothesService {
    Iterable<Clothes> getFiltered(ObjectFilter filter);

    Iterable<GalleryItem> getGalleryItems(int id, String url);

    Clothes insertClothes(ClothesDetails clothesDetails);

    Clothes getClothes(int id);

    byte[] getImage(String name);
}
