package com.home.fashion.service;

import com.home.fashion.core.ObjectFilter;
import com.home.fashion.model.ClothesSet;

public interface ClothesSetService {
    Iterable<ClothesSet> getFiltered(ObjectFilter filter);

    ClothesSet insertClothesSet(ClothesSet clothes);

    ClothesSet getClothesSet(int id);
}
