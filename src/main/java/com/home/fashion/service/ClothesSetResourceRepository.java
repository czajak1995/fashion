package com.home.fashion.service;

import com.home.fashion.model.ClothesSetResource;
import org.springframework.data.repository.CrudRepository;

public interface ClothesSetResourceRepository extends CrudRepository<ClothesSetResource, String> {
}
