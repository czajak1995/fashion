package com.home.fashion.service;

import com.home.fashion.model.ClothesResource;
import org.springframework.web.multipart.MultipartFile;

public interface UploadService {
    ClothesResource saveClothes(MultipartFile image);

    String saveClothesSet(MultipartFile resource);
}
