package com.home.fashion.service;

import com.home.fashion.model.ClothesResource;
import org.springframework.data.repository.CrudRepository;

public interface ClothesResourceRepository extends CrudRepository<ClothesResource, String> {
}
