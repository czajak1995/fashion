package com.home.fashion.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.List;

@Data
@Entity
@Table(name = "CLOTHES_SET_RESOURCE")
public class ClothesSetResource {
    @Id
    private String name;

    @Lob
    private byte[] resource;

    @JsonIgnore
    @ManyToMany(mappedBy = "resources")
    private List<ClothesSet> sources;

    public static ClothesSetResource emptyResource(String name) {
        ClothesSetResource resource = new ClothesSetResource();
        resource.setName(name);
        return resource;
    }
}
