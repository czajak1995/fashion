package com.home.fashion.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.List;

@Data
@Entity
@Table(name = "CLOTHES_RESOURCE")
public class ClothesResource {
    @Id
    private String name;

    @JsonIgnore
    private Integer width;

    @JsonIgnore
    private Integer height;

    @JsonIgnore
    private String dominantColor;

    @Lob
    private byte[] resource;

    @JsonIgnore
    @ManyToMany(mappedBy = "resources")
    private List<Clothes> sources;

    public static ClothesResource emptyResource(String name) {
        ClothesResource resource = new ClothesResource();
        resource.setName(name);
        return resource;
    }
}
