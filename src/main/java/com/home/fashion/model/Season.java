package com.home.fashion.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
@RequiredArgsConstructor
@Getter
public enum Season {
    Spring,
    Summer,
    Autumn,
    Winter,
    Rain,
    Snow,
    Hot,
    Heat;

    Season() {
        this.displayName = name();
    }

    private final String displayName;

    @JsonProperty("enumName")
    public String getName() {
        return this.name();
    }
}
