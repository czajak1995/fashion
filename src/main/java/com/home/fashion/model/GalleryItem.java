package com.home.fashion.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class GalleryItem {

    public GalleryItem(String name, double width, double height, String dominantColor, String url) {
        previewXxs = new ImageMetadata(String.format(url, name), width, height);
        previewXs = new ImageMetadata(String.format(url, name), width, height);
        previewS = new ImageMetadata(String.format(url, name), width, height);
        previewM = new ImageMetadata(String.format(url, name), width, height);
        previewL = new ImageMetadata(String.format(url, name), width, height);
        previewXl = new ImageMetadata(String.format(url, name), width, height);
        raw = new ImageMetadata(String.format(url, name), width, height);
        this.dominantColor = dominantColor;
    }

    @JsonProperty(value = "preview_xxs")
    private ImageMetadata previewXxs;

    @JsonProperty(value = "preview_xs")
    private ImageMetadata previewXs;

    @JsonProperty(value = "preview_s")
    private ImageMetadata previewS;

    @JsonProperty(value = "preview_m")
    private ImageMetadata previewM;

    @JsonProperty(value = "preview_l")
    private ImageMetadata previewL;

    @JsonProperty(value = "preview_xl")
    private ImageMetadata previewXl;

    @JsonProperty(value = "raw")
    private ImageMetadata raw;

    private String dominantColor;

}
