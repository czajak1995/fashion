package com.home.fashion.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
@RequiredArgsConstructor
@Getter
public enum Color {
    Red("#ff0000"),
    Green("#00ff00"),
    Pink("#fffff00"),
    Blue("#0000ff"),
    Brown("#00ffff"),
    Grey("#ffffff");

    Color() {
        this.displayName = name();
    }

    private final String displayName;

    @JsonProperty("enumName")
    public String getName() {
        return this.name();
    }
}
