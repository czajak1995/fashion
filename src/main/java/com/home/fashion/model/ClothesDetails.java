package com.home.fashion.model;

import lombok.Data;

import java.util.List;

@Data
public class ClothesDetails {
    private List<Color> colors;
    private ClothesType type;
    private List<Occasion> occasions;
    private List<Season> seasons;
    private List<String> tags;
    private Integer ownRank;
    private boolean isDisabled;
    //    private Place place;
    private List<String> resources;
    private String user;
}
