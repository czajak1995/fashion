package com.home.fashion.model.utils;

import javax.persistence.Entity;

public class
EntityUtils {

    public static String getTableName(Class<?> clazz) {
        if (!clazz.isAnnotationPresent(Entity.class)) {
            throw new IllegalArgumentException(String.format("Class %s does not exist in persistent context", clazz.getSimpleName()));
        }
        return clazz.getAnnotation(Entity.class).name();
    }
}
