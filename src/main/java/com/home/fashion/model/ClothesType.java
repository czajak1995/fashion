package com.home.fashion.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
@RequiredArgsConstructor
@Getter
public enum ClothesType {
    Tshirt("T-shirt"),
    Trousers,
    Sweater;

    ClothesType() {
        this.displayName = name();
    }

    private final String displayName;

    @JsonProperty("enumName")
    public String getName() {
        return this.name();
    }
}
