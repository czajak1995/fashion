package com.home.fashion.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.home.fashion.core.annotations.IgnoreFilter;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.CascadeType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.List;

@Data
@Entity
@Table(name = "CLOTHES_SET")
@EqualsAndHashCode(exclude = {"clothes"})
@ToString(exclude = {"clothes"})
public class ClothesSet {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @ElementCollection
    @JoinTable(name = "CLOTHES_SET_OCASSIONS", joinColumns = @JoinColumn(name = "clothes_id"))
    @Enumerated(EnumType.STRING)
    private List<Occasion> occasions;

    @ElementCollection
    @JoinTable(name = "CLOTHES_SET_SEASONS", joinColumns = @JoinColumn(name = "clothes_id"))
    @Enumerated(EnumType.STRING)
    private List<Season> seasons;

    @ElementCollection
    private List<String> tags;

    private Integer ownRank;

    private Integer othersRank;

    private Boolean isEmergency;

    @ManyToMany(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    @JoinTable(name = "CLOTHES_SET_REOURCES",
            joinColumns = @JoinColumn(name = "item_id"),
            inverseJoinColumns = @JoinColumn(name = "resource_id"))
    @IgnoreFilter
    private List<ClothesSetResource> resources;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "CLOTHES_SET_CLOTHES",
            joinColumns = @JoinColumn(name = "clothes_set_id"),
            inverseJoinColumns = @JoinColumn(name = "clothes_id"))
    @IgnoreFilter
    @JsonIgnoreProperties("clothesSets")
    private List<Clothes> clothes;

    @ManyToOne(fetch = FetchType.EAGER)
    @JsonIgnoreProperties(value = {"clothes", "clothesSets"})
    private User user;
}
