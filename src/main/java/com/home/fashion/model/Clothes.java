package com.home.fashion.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.home.fashion.core.annotations.IgnoreFilter;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.CascadeType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.List;

@Data
@Entity
@Table(name = "CLOTHES")
@EqualsAndHashCode(exclude = {"clothesSets"})
@ToString(exclude = {"clothesSets"})
public class Clothes {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @ElementCollection
    @JoinTable(name = "CLOTHES_COLORS", joinColumns = @JoinColumn(name = "clothes_id"))
    @Enumerated(EnumType.STRING)
    private List<Color> colors;

    @Enumerated(EnumType.STRING)
    private ClothesType type;

    @ElementCollection
    @JoinTable(name = "CLOTHES_OCCASIONS", joinColumns = @JoinColumn(name = "clothes_id"))
    @Enumerated(EnumType.STRING)
    private List<Occasion> occasions;

    @ElementCollection
    @JoinTable(name = "CLOTHES_SEASONS", joinColumns = @JoinColumn(name = "clothes_id"))
    @Enumerated(EnumType.STRING)
    private List<Season> seasons;

    @ElementCollection
    private List<String> tags;

    private Integer ownRank;

    private boolean isDisabled;

/*    @ManyToOne(fetch = FetchType.EAGER)
    private Place place;*/

    @ManyToMany(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    @JoinTable(name = "CLOTHES_RESOURCES",
            joinColumns = @JoinColumn(name = "item_id"),
            inverseJoinColumns = @JoinColumn(name = "resource_id"))
    @IgnoreFilter
    private List<ClothesResource> resources;

    @ManyToMany(mappedBy = "clothes")
    @JsonIgnoreProperties("clothes")
    private List<ClothesSet> clothesSets;

    @ManyToOne(fetch = FetchType.EAGER)
    @JsonIgnoreProperties(value = {"clothes", "clothesSets"})
    private User user;

}
