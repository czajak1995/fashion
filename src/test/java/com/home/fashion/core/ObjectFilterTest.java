/*
package com.home.fashion.core;

import com.google.common.collect.ImmutableMap;
import com.home.fashion.model.Clothes;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class ObjectFilterTest {

    @Test
    void getFilters() {
        Map<String, List<Object>> values = ImmutableMap.<String, List<Object>>builder()
                .put("name", Arrays.asList("xyz"))
                .put("unknown", Arrays.asList("unknown"))
                .build();
        Map<String, List<Object>> expectedFilters = ImmutableMap.<String, List<Object>>builder()
                .put("name", Arrays.asList("xyz"))
                .build();

        ObjectFilter objectFilter = new ObjectFilter(values);

        assertThat(objectFilter.getFilters(Clothes.class))
                .isEqualTo(expectedFilters);
    }

}
*/
