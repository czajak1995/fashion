package com.home.fashion.service;

import com.home.fashion.service.utils.QueryExecutorImpl;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class QueryExecutorImplTest {

    @Test
    void testToObjectLiteral() {
//        assertEquals("\'a\'", QueryExecutor.toObjectLiteral("a"));
//        assertEquals("1", QueryExecutor.toObjectLiteral(1));
//        assertEquals("\'a\',\'b\',\'c\'", QueryExecutor.toObjectLiteral(Arrays.asList("a", "b", "c")));
//        assertEquals("1,2,3", QueryExecutor.toObjectLiteral(Arrays.asList(1, 2, 3)));
//        assertEquals("1,2,3", QueryExecutor.toObjectLiteral(Arrays.asList(1, 2, 3)));
        assertEquals("1,2,3", QueryExecutorImpl.toObjectLiteral(new Integer[] {1, 2, 3}));
    }
}
