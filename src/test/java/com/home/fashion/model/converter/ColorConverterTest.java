/*
package com.home.fashion.model.converter;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.postgresql.core.BaseConnection;
import org.postgresql.jdbc.PgArray;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ColorConverterTest {

    @Mock
    private DataSource dataSource;

    private ColorConverter colorConverter;

    @BeforeEach
    void init() {
        MockitoAnnotations.initMocks(this);
        colorConverter = new ColorConverter(dataSource);
    }

    @Test
    void convertToEntityAttributeWhenEmptyPgArray() throws SQLException {
        PgArray pgArray = mock(PgArray.class);

        when(pgArray.getArray()).thenReturn(new Integer[0]);

        assertThat(colorConverter.convertToEntityAttribute(pgArray))
                .isEqualTo(Collections.emptyList());
    }

    @Test
    void convertToEntityAttributeWhenNullPgArray() {
        assertThat(colorConverter.convertToEntityAttribute(null))
                .isEqualTo(Collections.emptyList());
    }

    @Test
    void convertToEntityAttributeWhenNonEmptyPgArray() throws SQLException {
        PgArray pgArray = mock(PgArray.class);

        when(pgArray.getArray()).thenReturn(new Integer[]{1, 2});

        assertThat(colorConverter.convertToEntityAttribute(pgArray))
                .isEqualTo(Arrays.asList(1, 2));
    }

    @Test
    void convertToDatabaseColumnWhenEmptyList() throws SQLException {
        Connection connection = mock(Connection.class);
        BaseConnection baseConnection = mock(BaseConnection.class);
        List<Integer> colors = Collections.emptyList();

        when(dataSource.getConnection()).thenReturn(connection);
        when(connection.isWrapperFor(BaseConnection.class)).thenReturn(true);
        when(connection.unwrap(BaseConnection.class)).thenReturn(baseConnection);

        assertThat(colorConverter.convertToDatabaseColumn(colors).toString())
                .isEqualTo("{}");
    }

    @Test
    void convertToDatabaseColumnWhenNullList() throws SQLException {
        Connection connection = mock(Connection.class);
        BaseConnection baseConnection = mock(BaseConnection.class);

        when(dataSource.getConnection()).thenReturn(connection);
        when(connection.isWrapperFor(BaseConnection.class)).thenReturn(true);
        when(connection.unwrap(BaseConnection.class)).thenReturn(baseConnection);

        assertThat(colorConverter.convertToDatabaseColumn(null).toString())
                .isEqualTo("{}");
    }

    @Test
    void convertToDatabaseColumnWhenNonEmptyList() throws SQLException {
        Connection connection = mock(Connection.class);
        BaseConnection baseConnection = mock(BaseConnection.class);
        List<Integer> colors = Arrays.asList(1, 2);

        when(dataSource.getConnection()).thenReturn(connection);
        when(connection.isWrapperFor(BaseConnection.class)).thenReturn(true);
        when(connection.unwrap(BaseConnection.class)).thenReturn(baseConnection);

        assertThat(colorConverter.convertToDatabaseColumn(colors).toString())
                .isEqualTo("{1, 2}");
    }

    @Test
    void convertToDatabaseColumnWhenNonBaseConnection() throws SQLException {
        Connection connection = mock(Connection.class);
        List<Integer> colors = Arrays.asList(1, 2);

        when(dataSource.getConnection()).thenReturn(connection);
        when(connection.isWrapperFor(BaseConnection.class)).thenReturn(false);

        assertThat(colorConverter.convertToDatabaseColumn(colors))
                .isEqualTo(null);
    }

}
*/
